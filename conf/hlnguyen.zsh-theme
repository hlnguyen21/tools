local ret_status="%(?:%{$fg_bold[green]%}➜ :%{$fg_bold[red]%}➜ %s)"
#PROMPT='${ret_status}%{$fg_bold[green]%}%p %{$fg[cyan]%}%c %{$fg_bold[blue]%}$(git_prompt_info)%{$fg_bold[blue]%} % %{$reset_color%}'
ZSH_THEME_GIT_PROMPT_PREFIX="git:(%{$fg[red]%}"
ZSH_THEME_GIT_PROMPT_SUFFIX="%{$reset_color%}"
ZSH_THEME_GIT_PROMPT_DIRTY="%{$fg[blue]%}) %{$fg[yellow]%}✗%{$reset_color%}"
ZSH_THEME_GIT_PROMPT_CLEAN="%{$fg[blue]%})"

setprompt () {
    # Need this, so the prompt will work
    setopt prompt_subst

    # Let's load colors into our environment, then set them
    autoload -U colors && colors

    for COLOR in RED GREEN YELLOW BLUE WHITE BLACK; do
        eval PR_$COLOR='%{$fg_no_bold[${(L)COLOR}]%}'
        #eval PR_BOLD_$COLOR='%{$fg_bold[${(L)COLOR}]%}'
    done

    eval STATUSCOLOR="\${PR_${ESTATUS_COLOR}}"

    #PROMPT='${PR_RED}%n${PR_BOLD_RED}@${PR_RED}%m${PR_BOLD_RED}${PR_WHITE}%#%{${reset_color}%} '
    PROMPT='${PR_RED}hlnguyen21${PR_BOLD_RED}@${PR_RED}yoshirz${PR_BOLD_RED}${PR_WHITE}%#%{${reset_color}%} '
    PROMPT2='> '
}

setprompt
