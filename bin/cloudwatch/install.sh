#!/bin/bash
#install Custom Metrics dependencies
sudo apt-get update
sudo apt-get install -y ruby1.9.1   
sudo apt-get install -y libdatetime-perl
sudo apt-get update -y
sudo apt-get install -y unzip
sudo apt-get install -y libwww-perl libdatetime-perl
yes | sudo apt-get install dpkg

#download scripts
wget http://aws-cloudwatch.s3.amazonaws.com/downloads/CloudWatchMonitoringScripts-1.2.1.zip
yes | unzip CloudWatchMonitoringScripts-1.2.1.zip
rm CloudWatchMonitoringScripts-1.2.1.zip
#move credentials into script dir
sudo mv /home/ubuntu/credentials.csv /home/ubuntu/aws-scripts-mon/

crontab -l | grep -q 'cloudwatch'  && echo 'entry exists' || echo 'entry does not exist' | (crontab -l 2>/dev/null; echo "*/5 * * * *  ~/aws-scripts-mon/mon-put-instance-data.pl  --aws-credential-file=/home/ubuntu/aws-scripts-mon/credentials.csv --mem-util --disk-path=/mnt  --disk-space-util") | crontab -

#install cloudwatch API
curl -L https://bitbucket.org/dowdandassociates/aws_scripts/raw/master/install_aws_cli/install_cloudwatch.sh | sudo bash

#move credentials to API folder
sudo chmod 644 ~/aws-scripts-mon/credentials.csv 
sudo cp ~/aws-scripts-mon/credentials.csv  /opt/aws/CloudWatch/

#install Java
yes | sudo apt-get install openjdk-7-jdk

#install AWS API
curl "https://s3.amazonaws.com/aws-cli/awscli-bundle.zip" -o "awscli-bundle.zip"
unzip awscli-bundle.zip
sudo ./awscli-bundle/install -i /usr/local/aws -b /usr/local/bin/aws
rm -r awscli-bundle
rm -r awscli-bundle.zip

#configure the paths
export AWS_CLOUDWATCH_HOME=/opt/aws/CloudWatch
[[ ":$PATH:" != *":$AWS_CLOUDWATCH_HOME/bin:"* ]] && PATH="$AWS_CLOUDWATCH_HOME/bin:${PATH}"

JAVA_HOME=/usr/lib/jvm/java-7-openjdk-amd64
export JAVA_HOME
[[ ":$PATH:" != *":$JAVA_HOME/bin:"* ]] && PATH="$JAVA_HOME/bin:${PATH}"

AWS_CREDENTIAL_FILE=/opt/aws/CloudWatch/credentials.csv
export AWS_CREDENTIAL_FILE
[[ ":$PATH:" != *":$AWS_CREDENTIAL_FILE:"* ]] && PATH="$AWS_CREDENTIAL_FILE:${PATH}"

if  grep -q 'JAVA_HOME' ~/.bashrc; then #check if JAVA_HOME exist already
        echo "Matched" 
else 
        echo "No Match"
cat >> ~/.bashrc <<'EOF'
JAVA_HOME=/usr/lib/jvm/java-7-openjdk-amd64
export JAVA_HOME
[[ ":$PATH:" != *":$JAVA_HOME/bin:"* ]] && PATH="$JAVA_HOME/bin:${PATH}"

AWS_CREDENTIAL_FILE=/opt/aws/CloudWatch/credentials.csv
export AWS_CREDENTIAL_FILE
EOF
fi

ruby /home/ubuntu/alarms.rb