require 'rubygems'
require 'net/ssh'
require 'net/smtp'
require 'net/scp'

#get IPS
Ips = []
def readIPs()
	file = 'config.txt'
	File.readlines(file).each do |line|
		oneLine = line.split(" ")
		Ips.push(oneLine[1])
	end
	return Ips
end

#GET USERS
Users = []
def readUsers()
	file = 'config.txt'
	File.readlines(file).each do |line|
		oneLine = line.split(" ")
		Users.push(oneLine[3])
	end
	return Users
end

#GET KEYS
def readKeys()
	keys = []
	file = 'config.txt'
	File.readlines(file).each do |line|
		oneLine = line.split(" ")
		keys.push(oneLine[4])
	end
	return keys
end

#Get server name
def readNames()
	names = []
	file = 'config.txt'
	File.readlines(file).each do |line|
		oneLine = line.split(" ")
		names.push(oneLine[2])
	end
	return names
end

def readIndexes()
	i = []
	file = 'config.txt'
	File.readlines(file).each do |line|
		oneLine = line.split(" ")
		i.push(oneLine[0])
	end
	return i
end

def readPath()
	file = 'config.txt'
	return File.expand_path(File.dirname(file))
end

#now the script starts
#get all the IPs, usernames, and keys needed to connect to servers
HOST_ARR = readIPs()
USER_ARR = readUsers()
KEY_ARR = readKeys()
NAME_ARR = readNames()
INDEX_ARR = readIndexes()
PATH = readPath()

#do the sshing
passed=0
failed=0
INDEX_ARR.each do |i|

	begin
		puts
		puts
		Net::SSH.start(HOST_ARR[(i).to_i-1], USER_ARR[(i).to_i-1], :keys => KEY_ARR[(i).to_i-1]) do |ssh|
			puts i+"/"+(HOST_ARR.length).to_s
			#send installation folder 'cloudwatch' to server

			ssh.scp.upload! "#{PATH}/install.sh", "/home/ubuntu/"
			ssh.scp.upload! "#{PATH}/alarms.rb", "/home/ubuntu/"
			ssh.scp.upload! "#{PATH}/credentials.csv", "/home/ubuntu/"

			#run the installation script located in the'cloudwatch' DIR on said server
			system("ssh #{NAME_ARR[(i).to_i-1]} '. /home/ubuntu/install.sh' ")
			passed+=1
		end

	rescue Net::SSH::AuthenticationFailed => e
		puts
		puts i+"/"+(HOST_ARR.length).to_s
		puts "Connection Failed"
		puts NAME_ARR[(i).to_i-1]
		puts HOST_ARR[(i).to_i-1]
		puts
		failed+=1	

	rescue 
		puts
		puts i+"/"+(HOST_ARR.length).to_s
		puts "Connection Faillllled"
		puts NAME_ARR[(i).to_i-1]
		puts HOST_ARR[(i).to_i-1]
		puts
		failed+=1	

	end
end

puts
puts
puts "Passed: #{passed.to_s} / #{(HOST_ARR.length).to_s}"
puts "Failed: #{failed.to_s} / #{(HOST_ARR.length).to_s}"