README — ssh.rb

INSTALL CUSTOM METRICS TO SERVERS
The ssh.rb script SSHes to a list of BCC servers (in the config file) and performs custom AWSCloudwatch installation, Cloudwatch API installation and sets the Custom metric alarms

Running the script
	config.txt and ssh.rb must be in the same directory.  Open the terminal and cd to that directory.  To run it, type “ruby ssh.rb 

Config file
	Each line of the config file represents a BCC server that hosts a web service for a client.  To properly input a server into the config.txt file, follow the following rules.
For each line (server) of the file,
1-The first entry is an incremental value that represents the index of the server (first line is 1, second is 2, third is 3, etc.) The reason for this is to make it easier to know where the script is when it is going through all the servers, and allows it to differentiate between two servers with the same english name when it is outputting.
2- The second entry is the name of the server (in english)
3- The third entry is the username used to SSH to the server
4- The fourth entry is the keypair file and the path to it
5- The fifth entry is the IP of the server.

EVERY ENTRY ON A LINE MUST BE SEPARATED BY A SPACE

*If a server is removed, all incremental values must be adjusted as to make the output make sense.

Methods

readIPs(), readUsers(), readKeys(), readNames(), readIndexes()
	These methods wreathe config file and each one generates an array.  An index value in one array will represent information about a server. The same index in another array will be information about the same server.