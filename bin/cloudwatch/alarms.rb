#require 'rubygems'

EC2_INSTANCE_ID = %x(wget -q -O - http://instance-data/latest/meta-data/instance-id)
EC2_INSTANCE_NAME= %x(aws ec2 describe-tags  --filters Name=resource-id,Values=#{EC2_INSTANCE_ID}  Name=key,Values=Name --region us-east-1 --query Tags[].Value --output text)
EC2_INSTANCE_NAME.delete!("\n")

%x(JAVA_HOME=/usr/lib/jvm/java-7-openjdk-amd64;
export JAVA_HOME)
%x(AWS_CREDENTIAL_FILE=/opt/aws/CloudWatch/credentials.csv;
export AWS_CREDENTIAL_FILE)
#metric ids containing a different drive name
expt_ids = ['i-e708799d','i-81d754fb', 'i-5b91880a', 'i-583fe923']

#create Memory Utilization alarm
begin
	
	#set alarm for the Memory Utilization of instance 'id'
    %x(mon-put-metric-alarm MemUtilization:#{EC2_INSTANCE_NAME} --comparison-operator GreaterThanOrEqualToThreshold --metric-name MemoryUtilization --dimensions InstanceId=#{EC2_INSTANCE_ID} --threshold 75 --evaluation-periods 5 --namespace System/Linux --period 300  --statistic Average --unit Percent --alarm-actions arn:aws:sns:us-east-1:914746133600:Hai_Long)


rescue 
	puts
	puts "Error While Creating Alarm For:"
	puts EC2_INSTANCE_NAME
	puts
end


#create Disk Utilization alarms
begin

	if expt_ids.include? EC2_INSTANCE_ID
		#set alarm for the Disk Space Utilization of instance 'id' with Filesystem=/dev/xvda2
    	%x(mon-put-metric-alarm DiskSpaceUtilization:#{EC2_INSTANCE_NAME} --comparison-operator GreaterThanOrEqualToThreshold --metric-name DiskSpaceUtilization --dimensions "InstanceId=#{EC2_INSTANCE_ID}, Filesystem=/dev/xvda2, MountPath=/mnt" --threshold 95 --evaluation-periods 1 --namespace System/Linux --period 300  --statistic Average --unit Percent --alarm-actions arn:aws:sns:us-east-1:914746133600:Hai_Long)
	
	else
		#set alarm for the Disk Space Utilization of instance 'id' with Filesystem=/dev/xvdb
	  	%x(mon-put-metric-alarm DiskSpaceUtilization:#{EC2_INSTANCE_NAME} --comparison-operator GreaterThanOrEqualToThreshold --metric-name DiskSpaceUtilization --dimensions "InstanceId=#{EC2_INSTANCE_ID}, Filesystem=/dev/xvdb, MountPath=/mnt" --threshold 95 --evaluation-periods 1 --namespace System/Linux --period 300  --statistic Average --unit Percent --alarm-actions arn:aws:sns:us-east-1:914746133600:Hai_Long)

	end
rescue 
	puts
	puts "Error While Creating Alarm For:"
	puts EC2_INSTANCE_NAME
	puts
end

puts
puts