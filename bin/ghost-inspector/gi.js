//The script will be in charge of running the tests every morning, say 6am (cronjob)
//1. Must Run all tests in the suites 
//	1.1 Blacklist?
//2. Report if there are failures 
//	2.1 Reported to the ghost-inspector slack channel
//	2.2 Report structure:
//		Tested::
//		Suite Name: xxxxxxx  Tests Passed: xx/xx
//		Suite Name: xxxxxxx  Tests Passed: xx/xx
//		Suite Name: xxxxxxx  Tests Passed: xx/xx
//		Suite Name: xxxxxxx  Tests Passed: xx/xx



var GhostInspector = require('ghost-inspector')('fabbc02b117a51989a8d68878d526fbd6792ea96');
var BlackList = ["Enjoy"]
var sys = require('sys')
var exec = require('child_process').exec;

function puts(error, stdout, stderr) { sys.puts(stdout) }

GhostInspector.getSuites(function(err, suites){
    if(err) return console.log('Error: ' + err);

	for (i =0; i < suites.length; i++){
		if (BlackList.indexOf(suites[i].name) == -1 ){
			var options = { };
			
			GhostInspector.executeSuite(suites[i]._id, options, function(err, results, passing){
								
			    if(err) return console.log('Error: ' + err);
			    console.log(passing === true ? 'Passed' : 'Failed');

			    if (results != undefined){
				    GhostInspector.getResult(results[0]._id, function(err, result){
					    if(err) return console.log('Error: ' + err);

						GhostInspector.getSuite(result.test.suite, function(err, suite){
							
						    if(err) return console.log('Error: ' + err);
						    var name = suite.name;
						    var passCount = 0;
						    var totalCount = 0;

						    for(var j = 0; j<results.length;j++){
						    	if(results[j].passing){
						    		passCount++;
						    	}
						    	totalCount++;
							}

						    var string = "Suite Name: "+name+" ---- Tests Passed:  "+passCount+"/"+totalCount+"";
							var command = "curl --data '"+string+"' 'https://bencrudo.slack.com/services/hooks/slackbot?token=IGytizmO81ClAMIuHivaViWw&channel=%23ghost-inspector'";
							exec(command,puts);
						});
					});
				}
			});
		}
	}

});
