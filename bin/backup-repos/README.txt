README — backup-repos.rb

This script uses the BitBucket API to clone all the repositories of the SIGNED-IN user, it is possible to specify a specific owner and clone only his repos present in the SIGNED-IN repository. The script will also pull all corresponding branches in the repos. 

WARNING: The script keeps the local Repo in perfect sync with the remote Repo and thus will delete local branches if the corresponding remote branch is gone.  

Running the script
	Edit the config.txt file to configure the BitBucket login information (USERNAME, PASS), the local repository path (PATH) and the BitBucket Username you which to clone the repo from (TARGET_USER_REPO), if cloning only your repo then enter your username.

Methods

checkoutBranches(repo, loopError)
	This method prunes the local branches if they no longer exist in the remote repo as well as pulling all branches available in the remote repo

cloneRepo(repo, loopError)
	This method clones the repo
