begin
	gem "google_drive"
rescue Gem::LoadError
	%x(sudo gem install google_drive)
	Gem.clear_paths
end

begin
	gem "logger"
rescue Gem::LoadError
	%x(sudo gem install logger)
	Gem.clear_paths
end

begin
	gem "bitbucket_rest_api"
rescue Gem::LoadError
	%x(sudo gem install bitbucket_rest_api)
	Gem.clear_paths
end

require 'rubygems'
require 'bitbucket_rest_api'
require 'fileutils'
require 'logger'
require "google/api_client"
require "google_drive"

#Logger Class
class MyLog
	def initialize(path, user_repo)
		if @logger.nil?
			@logger = Logger.new "#{path}/repo_#{user_repo}_log.txt"
			@logger.level = Logger::DEBUG
			@logger.datetime_format = '%Y-%m-%d %H:%M:%S '
		end
	end

	def log()
		return @logger
	end
end

#Google Drive Class
# class Drive
# 	def initialize(path, user_repo)
# 		@path = path
# 		@user_repo = user_repo
# 		@session = GoogleDrive.saved_session("drive_credentials_config.json")

# 		#drive root collection
# 		@rootC = @session.root_collection

# 		#TODO: create the drive collection @bitbucketC so that the log file can be uploaded to it; waiting for google_drive gem devs
# 		#check if drive folder 'Bitbucket' exists, create if not
# 		# if @rootC.subcollection_by_title("Bitbucket").nil?
# 		#   @bitbucketC = @rootC.create_subcollection("Bitbucket")
# 		# else
# 		# 	@bitbucketC = @rootC.subcollection_by_title("Bitbucket")
# 		# end 
# 	end

# 	def report()
# 		#TODO: upload file to the @bitbucketC drive collection instead of to @root; waiting for google_drive gem devs

# 		if @session.file_by_title("repo_#{@user_repo}_log.txt").nil?
# 			@file = @session.upload_from_file("#{@path}/repo_#{@user_repo}_log.txt", "repo_#{@user_repo}_log.txt", convert: false)
# 		else
# 			@file = @session.file_by_title("repo_#{@user_repo}_log.txt")
# 			@file.update_from_file("#{@path}/repo_#{@user_repo}_log.txt", params = {:file_name => "repo_#{@user_repo}_log.txt"})
# 		end
# 	end
# end

#Backup Class
class Backup
	def initialize()
		params = readConfig()
	
		@username = params[0]
		@password = params[1]
		@path = params[2]
		@target_user_repo = params[3]

		@unsuccesful_repos = []
		@succesful_repos = []
		@black_list = []
		createPath()

		#Initialize Class Instances
		@bitbucket = BitBucket.new login:@username, password:@password
		@logger = MyLog.new(@path, @target_user_repo)
		# @drive = Drive.new(@path, @target_user_repo)
	end

	#Read config.txt file for parameters
	def readConfig()
		params = []
		file = 'config.txt'
		File.readlines(file).each do |line|
			oneParams = line.split(" ")
			params.push(oneParams[1])
		end
		return params
	end

	#Make repo dir if it doesn't exist; .../BitBucket/Repos
	def createPath()
		if !(File.directory? @path)
			FileUtils.mkdir_p @path
		end
		 if File.file?("#{@path}/repo_#{@target_user_repo}_log.txt")
	  		File.delete("#{@path}/repo_#{@target_user_repo}_log.txt")
	  	end
	end

	#Return object Bitbucket
	def bitbucket()
		return @bitbucket
	end

	#Return object MyLog
	def logger()
		return @logger
	end
	
	# #Return object Drive
	# def drive()
	# 	return @drive
	# end

	#Return variable @black_list
	def blackList()
		return @black_list
	end

	#Return variable @succesful_repos
	def succesfulRepos()
		return @succesful_repos
	end

	#Return variable @unsuccesful_repos
	def unsuccesfulRepos()
		return @unsuccesful_repos
	end
	#Return variable @path
	def path()
		return @path
	end
	#Return variable @target_user_repo
	def targetUserRepo()
		return @target_user_repo
	end

	#Add repo name to Succes
	def pushSuccesfulRepos(repo)
		@succesful_repos.push(repo)
	end

	#Add repo name to Failures
	def pushUnsuccesfulRepos(repo)
		@unsuccesful_repos.push(repo)
	end

	#Git clone the repo
	def cloneRepo(repo, bool)
		cmd = "cd #{@path}; git clone git@bitbucket.org:#{@target_user_repo}/#{repo}.git"
		system(cmd)
		@logger.log.info "Cloned Repo #{repo}"
		puts "Cloned Repo #{repo}"

		if bool
			checkoutBranches(repo, true)
		end
	end

	#Git checksout all the branches of given repo
	def checkoutBranches(repo, bool)
		@bitbucket.repos.branches(@target_user_repo, repo) do |branch|
			branchName = branch[0]
			cmd = "cd #{@path}/#{repo} && git fetch -p && for branch in `git branch -vv | grep ': gone]' | awk '{print $1}'`; do git branch -D $branch; done && git checkout -q #{branchName}"
			
			if !system (cmd)
				#error occured, delete repo and re-clone unless this was already done once (bool = true) in which case only delete repo
				if !bool
					cmd = "rm -rf #{@path}/#{repo}"
					system (cmd)
					@logger.log.warn "An issue occured, removing and re-cloning repo #{repo}"
					puts "An issue occured, removing and re-cloning repo #{repo}"
					cloneRepo(repo, true)
					return
				else
					cmd = "rm -rf #{@path}/#{repo}"
					system (cmd)
					@unsuccesful_repos.push(repo)
					@logger.log.error "An error occured checking-out branches for repo #{repo}, and it was removed"
					puts "An error occured checking-out branches for repo #{repo}, and it was removed"
					return
				end
			else
				@logger.log.info "Checkout Branch #{branchName} for #{repo}"
				puts "Checkout Branch #{branchName} for #{repo}"				
			end
		end
		@succesful_repos.push(repo)
	end
end

#Create instance of Backup
backup = Backup.new

begin
	#Clone Repos
	filtered = backup.bitbucket.repos.list.select {|hash| hash['owner'].include?(backup.targetUserRepo)}
	if filtered.empty?
		backup.logger.log.warn "The user #{backup.targetUserRepo} is not part of your Repositories"
		# backup.drive.report
		puts "The user #{backup.targetUserRepo} is not part of your Repositories"
	else
		backup.logger.log.info "Backing up #{filtered.size} Repositories"
		filtered.each do |repo|
			begin
				unless backup.blackList.include? repo.slug
					puts
					puts "Now Checking Repo: #{repo.slug}" 
				
					#Check if repo is already cloned; if FALSE then clone and check out branches 
					#ELSE cd into dir and checkout branches; if an error occures then delete dir and re-clone
					if !(File.directory? "#{backup.path}/#{repo.slug}")

						backup.cloneRepo(repo.slug, false)
						backup.checkoutBranches(repo.slug, false)			
					else

						backup.checkoutBranches(repo.slug, false)
					end
				end
			rescue
				backup.logger.log.error "Cloning/Checking-out Repo #{repo.slug} has failed"
				puts "Cloning/Checking-out Repo #{repo.slug} has failed"

				backup.pushUnsuccesfulRepos(repo.slug)
			end
		end
		 
		unless backup.unsuccesfulRepos.empty?
			puts
			puts "The following #{backup.unsuccesfulRepos.size} Repos were not cloned do to an error occuring: "
			backup.logger.log.info "The following #{backup.unsuccesfulRepos.size} Repos were not cloned do to an error occuring: #{backup.unsuccesfulRepos.each do |repo| puts repo end}"
			puts
		end

		unless backup.succesfulRepos.empty?
			puts
			puts "The following #{backup.succesfulRepos.size} Repos were cloned and checked-out: "
			backup.logger.log.info "The following #{backup.succesfulRepos.size} Repos were cloned and checked-out: #{backup.succesfulRepos.each do |repo| puts repo end}"
			puts
		end

		# backup.drive.report
	end
rescue
	backup.logger.log.fatal "An unexpected error occured!"
	# backup.drive.report
	puts
	puts "An unexpected error occured!"
	puts
end
